import java.util.Scanner;

public enum Option {
    ADDING_A_NEW_RECORD(1, "ADD"),
    SEARCH_BY_NAME(2, "FIND BY NAME"),
    SEARCH_BY_PHONE_NUMBER(3, "FIND BY PHONE NUMBER"),
    REMOVAL(4, "DELETE"),
    EXIT(5, "EXIT");

    private final int index;
    String description;

    Option(int index, String description) {
        this.index = index;
        this.description = description;
    }

    public int getIndex() {
        return index;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static Option numberToOption(int number) {
        for (Option option : Option.values()) {
            if (option.getIndex() == number) {
                return option;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return getDescription();
    }

//    public static void main(String[] args) {
//        System.out.println("Wybierz opcje");
//        for(Option opt : Option.values()) {
//            System.out.println(opt.ordinal() + " - " + opt.toString());
//        }
//
//        Scanner sc = new Scanner(System.in);
//        int opcja = sc.nextInt();
//        if (opcja < Option.values().length) {
//            Option wybranaOpcja = Option.values()[opcja];
//
//            System.out.println("Wybrno opcje " + wybranaOpcja.toString());
//        } else {
//
//        }
//
//    }
}

