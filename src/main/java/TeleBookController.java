import java.util.Scanner;

public class TeleBookController {

    private TeleBook teleBook = new TeleBook();
    private Scanner input = new Scanner(System.in);


    /**
     * Main program loop
     */
    public void loop() {
        System.out.println("Choose option: ");
        Scanner sc = new Scanner(System.in);
        Option selectedOption = null;
        do {
            showOptions();
            try {
                int input = sc.nextInt();
                sc.nextLine();
                selectedOption = chooseOption(input);
                executeOption(selectedOption);
            } catch (Exception e) {
                System.out.println("Invalid number");
                loop();
            }
        } while (selectedOption != Option.EXIT);
    }

    /**
     * Shows available options to user
     */
    private void showOptions() {
        for (Option option : Option.values()) {
            System.out.println(option.getIndex() + " " + option);
        }
    }

    /**
     * Converts number to {@Link Option}
     * @param optionAsNumber
     * @return Option object or null if option number is not valid
     */
    private Option chooseOption(int optionAsNumber) {
        return Option.numberToOption(optionAsNumber);
    }

    /**
     * Executes selected option
     * @param option
     */
    private void executeOption(Option option) {
        switch (option) {
            case ADDING_A_NEW_RECORD:
                addContact();
                break;
            case SEARCH_BY_NAME:
                searchByName();
                break;
            case SEARCH_BY_PHONE_NUMBER:
                searchByTelephone();
                break;
            case REMOVAL:
                delete();
                break;
            case EXIT:
                close();
                break;
        }
    }

    /**
     * Asks user for contact name and removes {{@link Contact}}
     */
    private void delete() {
        System.out.println("Enter name: ");
        String name = input.nextLine();
        teleBook.deleteContact(name);
    }

    /**
     * Asks user for phone number or its fragment and prints matched {@link Contact}s
     */
    private void searchByTelephone() {
        boolean isCorrect = true;
        String phoneNo;
        do {
            System.out.println("Enter phone number: ");
            phoneNo = input.nextLine();
            try {
                Integer.parseInt(phoneNo);
                isCorrect = true;
            } catch (Exception e) {
                System.out.println("Invalid phone number");
            }
        } while (isCorrect == false);

        teleBook.search(phoneNo);
    }

    /**
     * Asks user for name or its fragment and prints matched {@link Contact}s
     */
    private void searchByName() {
        System.out.println("Enter name: ");
        String name = input.nextLine();
        teleBook.search(name);
    }

    /**
     * Asks user for name and phone number, creates Contact and prints it
     */
    private void addContact() {
        System.out.println("Enter name: ");
        String name = input.nextLine();

        boolean isCorrect = true;
        String phoneNo;
        do {
            System.out.println("Enter phone number: ");
            phoneNo = input.nextLine();
            if (phoneNo.length() != 9) {
                System.out.println("Invalid phone number");
                isCorrect = false;
                continue;
            }
            try {
                Integer.parseInt(phoneNo);
                isCorrect = true;
            } catch (Exception e) {
                System.out.println("Invalid phone number");
            }
        } while (isCorrect == false);


        Contact contact = new Contact(name, phoneNo);
        teleBook.addContact(contact);
    }

    /**
     * Exits the program
     */
    private void close() {
        ContactsWriter contactsWriter = new ContactsWriter();
        contactsWriter.write(teleBook.getContacts());
        //obsługa zamykania programu
        System.exit(0);
    }

}
