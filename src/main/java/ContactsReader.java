import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ContactsReader {


    public Map<String, Contact> load() throws FileNotFoundException {
        JSONParser parser = new JSONParser();
        Map<String, Contact> contacts = new HashMap<String, Contact>();

        try {
            Object contact = parser.parse(new FileReader("contacts.json"));
            JSONObject jsonObject = (JSONObject) contact;
            System.out.println(jsonObject);
            Contact contact1;

            for (Object name : jsonObject.keySet()) {
                String nameStr = (String) name;
                JSONObject jsonObject1 = (JSONObject) jsonObject.get(nameStr);
                String nameToContact = (String) jsonObject1.get("name");
                String phoneNoToContact = (String) jsonObject1.get("phone_number");
                contact1 = new Contact(nameToContact, phoneNoToContact);
                contacts.put(nameToContact, contact1);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contacts;
    }
}
