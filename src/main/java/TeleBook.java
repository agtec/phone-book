import java.io.FileNotFoundException;
import java.util.Map;
import java.util.TreeMap;

public class TeleBook {

    private Map<String, Contact> contacts = new TreeMap<>();

    public TeleBook() {
       // this.contacts = contacts;
        ContactsReader contactsReader = new ContactsReader();
        try {
            this.contacts = contactsReader.load();
        } catch (Exception e) {

        }

    }

    public Map<String, Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Map<String, Contact> contacts) {
        this.contacts = contacts;
    }

    public void addContact(Contact contact) {
        contacts.put(contact.getName(), contact);
        System.out.println("Added: " + contact);
    }

    public void deleteContact(String name) {
        int foud = 0;
        if (contacts.size() == 0) {
            System.out.println("The contact list is empty");
        } else {
            for (String contactName : contacts.keySet()) {
                if (contacts.get(contactName).getName().equalsIgnoreCase(name)) {
                    contacts.remove(contactName);
                    System.out.println("Deleted: " + contacts.get(name));
                    foud++;
                }
            }
        }
        if (foud == 0) {
            System.out.println("Contact not foud");
        }
    }

    public void search(String term) {
        term = term.toLowerCase();
        int found = 0;
        for (String contactName : contacts.keySet()) {
            if (contacts.get(contactName.toLowerCase()).getPhoneNo().contains(term) || contacts.get(contactName.toLowerCase()).getName().contains(term)) {
                System.out.println("Name: " + contactName + " phone number: " + contacts.get(contactName));
                found++;
            }
        }
        if (found == 0) {
            System.out.println("Contact not foud");
        }
    }

}
