import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class ContactsWriter {

    public void write(Map<String, Contact> map) {
        JSONObject obj = new JSONObject();

        for (String name : map.keySet()) {
            Contact contact = map.get(name);

            JSONObject contactJSON = new JSONObject();
            contactJSON.put("name", contact.getName());
            contactJSON.put("phone_number", contact.getPhoneNo());
            obj.put(name, contactJSON);
        }

        try (FileWriter file = new FileWriter("contacts.json")) {

            file.write(obj.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print(obj);
    }
}
